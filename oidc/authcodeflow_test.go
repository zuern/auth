// Copyright 2021 Kevin Zuern. All rights reserved.

package oidc_test

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/stretchr/testify/require"
	"gitlab.com/zuern/auth/oidc"
)

var logFunc = func(msg string, args ...interface{}) { fmt.Printf(msg, args...) }

func TestOIDCLoginFlow(t *testing.T) {
	require := require.New(t)

	const clientID = "client"

	// op is the testing openid provider.
	op := newOpenIDProvider(clientID)
	opSrv := httptest.NewServer(op)
	defer opSrv.Close()
	op.url = opSrv.URL

	// client is the oidc client app.
	client := &oidcClient{}
	clientSrv := httptest.NewServer(client)
	defer clientSrv.Close()
	redirectURL := clientSrv.URL + "/callback"

	cancelCtx, cf := context.WithCancel(context.Background())
	defer cf()

	authStore := &memStore{}
	auth, err := oidc.NewAuthCodeFlow(
		cancelCtx,
		authStore,
		opSrv.URL+"/",
		clientID,
		"secret",
		redirectURL,
		[]string{"openid"},
		5*time.Minute,
		logFunc,
	)
	require.NoError(err)
	require.NotNil(auth)
	client.auth = auth

	// Simulate a login flow.
	httpClient := &http.Client{
		CheckRedirect: func(_ *http.Request, _ []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	// Go to the login page.
	req, err := http.NewRequest("GET", clientSrv.URL+"/login", nil)
	require.NoError(err)

	// Check we're redirected to OpenID Provider (OP)
	resp, err := httpClient.Do(req)
	require.NoError(err)
	require.Equal(307, resp.StatusCode)
	req.URL, _ = url.Parse(resp.Header.Get("Location"))
	require.Contains(req.URL.String(), opSrv.URL)

	// Go to the OP auth endpoint to "log in" and check redirected to client app
	// after.
	resp, err = httpClient.Do(req)
	require.NoError(err)
	require.Equal(302, resp.StatusCode)
	req.URL, _ = url.Parse(resp.Header.Get("Location"))
	require.Contains(req.URL.String(), redirectURL)

	// Client should set cookie with session token and then redirect to "/".
	sent := time.Now()
	resp, err = httpClient.Do(req)
	require.NoError(err)
	require.Len(resp.Cookies(), 1)
	cookie := resp.Cookies()[0] // mmm cookies.
	require.Equal("session", cookie.Name)
	require.True(cookie.HttpOnly)
	require.True(cookie.Secure)
	// cookie should expire within the configured time.
	require.True(time.Since(sent) < 5*time.Second)
	require.Equal(307, resp.StatusCode)
	require.Equal("/", resp.Header.Get("Location"))

	// Check that the session exists on the auth store, and that the user info is
	// correct.
	req.AddCookie(cookie)
	sessionID := cookie.Value
	require.Equal(sessionID, auth.GetSession(req))
	userID, err := authStore.GetSession(context.Background(), sessionID)
	require.NoError(err)
	require.Equal("12345", userID)

	// check (indirectly) that the AddUser call to the oidc.AuthStore had all the
	// correct info.
	user := authStore.Users[userID]
	require.Equal("12345", user.id)
	require.Equal("Jane", user.given)
	require.Equal("Doe", user.family)
	require.Equal("janedoe@example.com", user.email)

	// Simulate a logout.
	req, err = http.NewRequest("GET", clientSrv.URL+"/logout", nil)
	req.AddCookie(cookie)
	require.NoError(err)
	resp, err = httpClient.Do(req)
	require.NoError(err)
	// Check the session cookie was wiped.
	require.Len(resp.Cookies(), 1)
	cookie = resp.Cookies()[0]
	require.Equal("session", cookie.Name)
	require.True(cookie.HttpOnly)
	require.True(cookie.Secure)
	require.Equal("", cookie.Value)
	require.True(cookie.Expires.Before(time.Now()))

	// Check for redirect to the OP's end_session_endpoint.
	require.Equal(307, resp.StatusCode)
	require.Equal(opSrv.URL+"/endSession", resp.Header.Get("Location"))
	// Follow the redirect to fully log out.
	req.URL, _ = url.Parse(resp.Header.Get("Location"))
	resp, err = httpClient.Do(req)
	require.NoError(err)
	require.Equal(200, resp.StatusCode)
	bytes, err := io.ReadAll(resp.Body)
	require.NoError(err)
	require.Contains(string(bytes), "You are now logged out at the OP")
}

func TestNewAuthCodeFlowErrs(t *testing.T) {
	require := require.New(t)

	// Check empty client id is rejected.
	cancelCtx, cf := context.WithCancel(context.Background())
	defer cf()
	authStore := &memStore{}
	auth, err := oidc.NewAuthCodeFlow(
		cancelCtx,
		authStore,
		"http://127.0.0.1/issuerURL",
		"", // empty client id.
		"secret",
		"http://127.0.0.1/redirectURL",
		[]string{"openid"},
		5*time.Minute,
		logFunc,
	)
	require.Error(err)
	require.Nil(auth)
	require.Contains(err.Error(), "empty client id")

	// An error is returned when discovery endpoint is not reachable.
	auth, err = oidc.NewAuthCodeFlow(
		cancelCtx,
		authStore,
		"http://127.0.0.1/issuerURL",
		"clientid",
		"secret",
		"http://127.0.0.1/redirectURL",
		[]string{"openid"},
		5*time.Minute,
		logFunc,
	)
	require.Error(err)
	require.Nil(auth)
	require.Contains(err.Error(), "discovery failed")
}

// oidcClient simulates the HTTP endpoints of an OIDC-enabled client
// application using the oidc package to handle auth.
type oidcClient struct {
	auth *oidc.AuthCodeFlow
}

func (c *oidcClient) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	switch req.URL.Path {
	case "/login":
		c.auth.HandleLoginRedirect(res, req)
	case "/callback":
		c.auth.HandleCallback(res, req)
	case "/logout":
		c.auth.HandleLogout(res, req)
	default:
		panic(req.URL)
	}
}

// openidProvider simulates an OpenID Provider for testing.
type openidProvider struct {
	url      string
	clientID string

	privateKey *rsa.PrivateKey
	publicKey  crypto.PublicKey
	jwkPublic  jwk.Key
	jwkPrivate jwk.Key
	jwkKeySet  jwk.Set
}

func newOpenIDProvider(clientID string) *openidProvider {
	op := &openidProvider{clientID: clientID}
	// For testing purposes create a JWK key to sign with.
	var err error
	if op.privateKey, err = rsa.GenerateKey(rand.Reader, 2048); err != nil {
		panic(err)
	}
	op.publicKey = op.privateKey.Public()
	if op.jwkPublic, err = jwk.New(op.publicKey); err != nil {
		panic(err)
	}
	if err = op.jwkPublic.Set(jwk.AlgorithmKey, jwa.RS256); err != nil {
		panic(err)
	}
	if op.jwkPrivate, err = jwk.New(op.privateKey); err != nil {
		panic(err)
	}
	if err = op.jwkPublic.Set(jwk.KeyIDKey, "my-awesome-key"); err != nil {
		panic(err)
	}
	if err = op.jwkPrivate.Set(jwk.KeyIDKey, "my-awesome-key"); err != nil {
		panic(err)
	}
	op.jwkKeySet = jwk.NewSet()
	op.jwkKeySet.Add(op.jwkPublic)
	return op
}

func (op *openidProvider) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	switch req.URL.Path {
	case "/.well-known/openid-configuration":
		op.handleWellKnown(res, req)
	case "/authorize":
		op.handleAuthorize(res, req)
	case "/token":
		op.handleToken(res, req)
	case "/userinfo":
		op.handleUserInfo(res, req)
	case "/jwks":
		op.handleJwks(res, req)
	case "/endSession":
		op.handleEndSession(res, req)
	default:
		panic(req.URL.Path)
	}
}

func (op *openidProvider) handleWellKnown(res http.ResponseWriter, _ *http.Request) {
	res.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(res, strings.ReplaceAll(`{
   "authorization_endpoint": "XX/authorize",
   "token_endpoint":         "XX/token",
   "userinfo_endpoint":      "XX/userinfo",
   "jwks_uri":               "XX/jwks",
   "end_session_endpoint":   "XX/endSession"
  }`, "XX", op.url))
}

func (op *openidProvider) handleAuthorize(res http.ResponseWriter, req *http.Request) {
	redirectURL := req.URL.Query().Get("redirect_uri")
	state := req.URL.Query().Get("state")
	const code = "one-time-code"

	// pretend the user-agent logged in and just go ahead with the callback.
	res.Header().Set("Location", fmt.Sprintf("%s?code=%s&state=%s", redirectURL, code, state))
	res.WriteHeader(http.StatusFound)
}

func (op *openidProvider) handleToken(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json")

	idTok := jwt.New()
	for k, v := range map[string]interface{}{
		// Required
		"iss":         op.url,
		"sub":         "12345",
		"aud":         op.clientID,
		"exp":         time.Now().UTC().Add(3600 * time.Second).Unix(),
		"iat":         time.Now().UTC().Unix(),
		"name":        "Jane Doe",
		"given_name":  "Jane",
		"family_name": "Doe",
		"email":       "janedoe@example.com",
		"picture":     "http://example.com/janedoe/me.jpg",
	} {
		if err := idTok.Set(k, v); err != nil {
			panic(err)
		}
	}

	signedIDTok, err := jwt.Sign(idTok, jwa.RS256, op.jwkPrivate)
	if err != nil {
		panic(err)
	}

	fmt.Fprintf(res, `{
	"access_token": "access_token_here",
	"token_type": "Bearer",
	"expires_in": 3600,
	"id_token": %q
}`, string(signedIDTok))
}

func (op *openidProvider) handleUserInfo(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(res, `  {
   "sub": "12345",
   "name": "Jane Doe",
   "given_name": "Jane",
   "family_name": "Doe",
   "preferred_username": "j.doe",
   "email": "janedoe@example.com",
   "picture": "http://example.com/janedoe/me.jpg"
  }`)
}

func (op *openidProvider) handleJwks(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json")
	bytes, err := json.Marshal(op.jwkKeySet)
	if err != nil {
		panic(err)
	}
	fmt.Fprintln(res, string(bytes))
}

func (op *openidProvider) handleEndSession(res http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(res, "You are now logged out at the OP")
}
