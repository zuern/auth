// Copyright 2021 Kevin Zuern. All rights reserved.

package oidc

import "context"

// AuthStore is the interface used by AuthCodeFlow to persist sessions and user
// data.
type AuthStore interface {
	// AddUser adds a user to the store.
	AddUser(ctx context.Context, id, email, given, family, picture string) error
	// UserExists returns true if the user exists and false if not. Error is
	// returned when the request failed.
	UserExists(ctx context.Context, id string) (exists bool, err error)
	// CreateSession creates a session for the given user.
	CreateSession(ctx context.Context, userID string) (sessionID string, err error)
	// GetSession returns the user id that the session is for or the empty string
	// if the session ID is unknown or expired.
	GetSession(ctx context.Context, sessionID string) (userID string, err error)
	// DeleteSession given an ID. If the ID is not found, no error is returned.
	DeleteSession(ctx context.Context, sessionID string) error
}
