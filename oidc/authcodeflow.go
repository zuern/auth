// Copyright 2021 Kevin Zuern. All rights reserved.

package oidc

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/lestrrat-go/jwx/jwt/openid"
	"gitlab.com/zuern/kit/id"
	"golang.org/x/oauth2"
)

// https://openid.net/specs/openid-connect-core-1_0.html
// reference materials: https://developers.google.com/identity/protocols/oauth2/openid-connect

type AuthCodeFlow struct {
	issuerURL     string
	sessionExpiry time.Duration
	config        *oauth2.Config
	oidcConfig    wellKnownResponse
	keySet        jwk.Set // Set of keys used by auth server to sign tokens.
	store         AuthStore
	stateCache    AuthStateCache
	logErr        func(string, ...interface{})
}

// NewAuthCodeFlow creates a new OIDC client using store for persistence
// and discovering openid server configuration from the issuerURL. Scopes MUST
// contain "openid". If logErrorFunc is nil, unexpected errors like invalid
// tokens or those from misbehaving OpenID Providers will not be logged.
func NewAuthCodeFlow(
	ctx context.Context,
	store AuthStore,
	issuerURL, clientID, clientSecret string,
	redirectURL string,
	scopes []string,
	sessionExpiry time.Duration,
	logErrorf func(format string, args ...interface{}),
) (*AuthCodeFlow, error) {
	if clientID == "" {
		return nil, fmt.Errorf("oidc configuration cannot have empty client id")
	}
	a := &AuthCodeFlow{
		store:         store,
		sessionExpiry: sessionExpiry,
		config: &oauth2.Config{
			RedirectURL:  redirectURL,
			ClientID:     clientID,
			ClientSecret: clientSecret,
			Scopes:       scopes,
		},
		stateCache: &memStateCache{states: map[string]int64{}},
		logErr:     logErrorf,
	}
	err := a.discoverConfiguration(ctx, issuerURL)
	if err != nil {
		a = nil
	}
	return a, err
}

// SetStateCache overrides the default in-memory cache with a custom implementation.
func (a *AuthCodeFlow) SetStateCache(cache AuthStateCache) {
	a.stateCache = cache
}

// GetSession extracts a session id from a cookie in the request.
func (a *AuthCodeFlow) GetSession(req *http.Request) string {
	var sessionID string
	if cookie, _ := req.Cookie("session"); cookie != nil {
		sessionID = cookie.Value
	}
	return sessionID
}

// HandleLoginRedirect is an http.HandlerFunc which redirects the user agent to
// the OpenID Provider to log in.
func (a *AuthCodeFlow) HandleLoginRedirect(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	state := id.New()
	if err := a.stateCache.Put(state); err == nil {
		http.Redirect(w, r, a.config.AuthCodeURL(state), http.StatusTemporaryRedirect)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *AuthCodeFlow) badRequest(w http.ResponseWriter) {
	http.Error(w, "400 Bad Request", http.StatusBadRequest)
}

func (a *AuthCodeFlow) internalServerError(w http.ResponseWriter, err error) {
	if err != nil {
		a.errorf("%s\n", err)
	}
	http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
}

// HandleCallback is an http.HandlerFunc to process the one-time code from the
// user agent and exchange it for an ID token and user info. After that a
// session is created for the user and they are redirected to the redirectURL.
func (a *AuthCodeFlow) HandleCallback(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	state, code := r.FormValue("state"), r.FormValue("code")

	// Check state token to prevent CSRF.
	if has, err := a.stateCache.Check(state); err != nil || !has {
		a.badRequest(w)
		return
	}

	ctx := r.Context()
	token, err := a.config.Exchange(ctx, code)
	if err != nil {
		a.errorf("exchange code for auth token failed: %s\n", err)
		a.badRequest(w)
		return
	}
	if !token.Valid() {
		a.errorf("invalid auth token: %s\n", err)
		a.badRequest(w)
		return
	}
	idTokenBytes, _ := token.Extra("id_token").(string)

	// Parse and verify signature of JWT.
	idToken, err := jwt.Parse(
		[]byte(idTokenBytes),
		jwt.WithKeySet(a.keySet),
		jwt.WithToken(openid.New()), // parse as openid token
	)
	if err != nil {
		a.errorf("parse/verify id token failed: %s\n", err)
		a.badRequest(w)
		return
	}

	// TODO if sig is from an unknown jwk.Key ID, re-fetch JWKs from a.jwksURI.
	// if a.keySet, err = jwk.Fetch(ctx, a.jwksURI); err != nil {
	// 	err = fmt.Errorf("fetch JWKS of auth server: %w", err)
	// }

	// https://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation
	if err = jwt.Validate(
		idToken,
		jwt.WithIssuer(a.issuerURL),         // 2. validate iss
		jwt.WithAudience(a.config.ClientID), // 3. validate aud
		jwt.WithClock(jwt.ClockFunc(time.Now)),
	); err != nil {
		a.errorf("invalid id token: %s\n", err)
		a.badRequest(w)
		return
	}

	// TODO May need to fetch missing info via userinfo endpoint. Could be made
	// configurable.
	var sub, email, given, family, picture string
	if tok, ok := idToken.(openid.Token); ok {
		sub = tok.Subject()
		email = tok.Email()
		given = tok.GivenName()
		family = tok.FamilyName()
		picture = tok.Picture()
	}

	userExists, err := a.store.UserExists(ctx, sub)
	if err != nil {
		a.internalServerError(w, fmt.Errorf("check user exists: %w", err))
		return
	}
	if !userExists {
		if err = a.store.AddUser(ctx, sub, email, given, family, picture); err != nil {
			a.internalServerError(w, fmt.Errorf("add user: %w", err))
			return
		}
	}

	var sessionID string
	if sessionID, err = a.store.CreateSession(ctx, sub); err != nil {
		a.internalServerError(w, fmt.Errorf("create a session for user: %w", err))
		return
	}
	a.setSessionCookie(w, sessionID, a.sessionExpiry)
	// TODO should make final redirect configurable.
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}

func (a *AuthCodeFlow) HandleLogout(w http.ResponseWriter, r *http.Request) {
	redirectTo := "/"
	if session := a.GetSession(r); session != "" {
		a.setSessionCookie(w, "", -1) // unset the session cookie.
		if err := a.store.DeleteSession(r.Context(), session); err != nil {
			w.WriteHeader(500)
		}
		// Also log out at OP.
		if a.oidcConfig.EndSessionEndpoint != "" {
			redirectTo = a.oidcConfig.EndSessionEndpoint
		}
	}
	http.Redirect(w, r, redirectTo, http.StatusTemporaryRedirect)
}

func (a *AuthCodeFlow) discoverConfiguration(ctx context.Context, issuerURL string) (err error) {
	if n := len(issuerURL); n > 0 && issuerURL[n-1] == '/' {
		issuerURL = issuerURL[:n-1]
	}
	a.issuerURL = issuerURL
	issuerURL += "/.well-known/openid-configuration"
	defer func() {
		if err != nil {
			err = fmt.Errorf("oidc configuration discovery failed: %w", err)
		}
	}()
	req, err := http.NewRequestWithContext(ctx, "GET", issuerURL, nil)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("discovery endpoint returned %d", resp.StatusCode)
	}
	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bytes, &a.oidcConfig); err != nil {
		return err
	}
	a.config.Endpoint.AuthURL = a.oidcConfig.AuthorizationEndpoint
	a.config.Endpoint.TokenURL = a.oidcConfig.TokenEndpoint
	if a.keySet, err = jwk.Fetch(ctx, a.oidcConfig.JwksURI); err != nil {
		err = fmt.Errorf("fetch JWKS of auth server: %w", err)
	}

	return err
}

func (a *AuthCodeFlow) setSessionCookie(resp http.ResponseWriter, sessionID string, expires time.Duration) {
	http.SetCookie(resp, &http.Cookie{
		Name:     "session",
		Value:    sessionID,
		Secure:   true,
		HttpOnly: true,
		MaxAge:   int(expires.Seconds()),
	})
}

func (a *AuthCodeFlow) errorf(msg string, args ...interface{}) {
	if a.logErr != nil {
		a.logErr("oidc err: " + fmt.Sprintf(msg, args...))
	}
}
