// Copyright 2021 Kevin Zuern. All rights reserved.

package oidc

import (
	"sync"
	"time"
)

// AuthStateCache is a cache to store OIDC state tokens. These tokens are used
// only during the login process and do not need to be stored long term. A
// cache cleanup or expiry policy may be used to avoid the cache growing
// unnecessarily with stale tokens from incomplete/abandoned login flows.
type AuthStateCache interface {
	// Put a token into the cache.
	Put(token string) error
	// Check if a token exists. After check is called once, the token will not be
	// used again and may be discarded.
	Check(token string) (bool, error)
}

type memStateCache struct {
	states       map[string]int64
	stateCleanAt int64
	lock         sync.Mutex
}

func (m *memStateCache) Put(token string) error {
	now := time.Now()
	unix := now.UnixNano()

	m.lock.Lock()
	m.states[token] = unix
	if m.stateCleanAt <= unix {
		for k, expiry := range m.states {
			if expiry <= m.stateCleanAt {
				delete(m.states, k)
			}
		}
		const stateExpiration = time.Hour
		m.stateCleanAt = now.Add(stateExpiration).UnixNano()
	}
	m.lock.Unlock()
	return nil
}

func (m *memStateCache) Check(token string) (bool, error) {
	m.lock.Lock()
	_, has := m.states[token]
	delete(m.states, token)
	m.lock.Unlock()
	return has, nil
}
