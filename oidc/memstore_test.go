// Copyright 2021 Kevin Zuern. All rights reserved.

package oidc_test

import "context"

type user struct {
	id      string
	email   string
	given   string
	family  string
	picture string
}

// memStore is an in-memory implementation of oidc.AuthStore for testing
// purposes.
type memStore struct {
	Users          map[string]*user
	Sessions       map[string]string
	userToSessions map[string][]string
}

/*
	Deliberately naive implementation, minimal error handling, can panic with
	bad input. Just something simple to power the tests.
*/

func (m *memStore) AddUser(_ context.Context, id, email, given, family, picture string) error {
	u := &user{id, email, given, family, picture}
	if m.Users == nil {
		m.Users = map[string]*user{u.id: u}
	} else {
		m.Users[u.id] = u
	}
	return nil
}

func (m *memStore) UserExists(ctx context.Context, id string) (bool, error) {
	u := m.Users[id]
	return u != nil, nil
}

func (m *memStore) CreateSession(_ context.Context, userID string) (sessionID string, err error) {
	sessionID = userID // simple and NOT good outside of testing.
	if m.Sessions == nil {
		m.Sessions = map[string]string{sessionID: userID}
		m.userToSessions = map[string][]string{sessionID: {userID}}
	} else {
		m.Sessions[sessionID] = userID
		m.userToSessions[userID] = append(m.userToSessions[userID], sessionID)
	}
	return
}

func (m *memStore) GetSession(_ context.Context, id string) (userID string, err error) {
	if m.Sessions != nil {
		userID = m.Sessions[id]
	}
	return
}

func (m *memStore) DeleteSession(_ context.Context, sessionID string) error {
	if m.Sessions == nil {
		return nil
	}
	uid := m.Sessions[sessionID]
	delete(m.Sessions, sessionID)
	if uid == "" {
		return nil
	}
	for i, s := range m.userToSessions[uid] {
		if s != sessionID {
			continue
		}
		l := m.userToSessions[uid]
		if len(l) > 1 {
			l[len(l)-1], l[i] = l[i], l[len(l)-1]
		}
		m.userToSessions[uid] = l[:len(l)-1]
	}
	return nil
}
