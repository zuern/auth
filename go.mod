module gitlab.com/zuern/auth

go 1.16

require (
	github.com/lestrrat-go/jwx v1.2.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/zuern/kit v0.1.3
	golang.org/x/oauth2 v0.0.0-20210622215436-a8dc77f794b6
)
